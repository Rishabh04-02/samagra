# How to run
Run the following command through terminal and observe the output through web api and terminal -

	python3 api.py

## Available routes
	/ - sample home page
	/get?idd=1 - returns data from mongodb database
	/start - starts the web scrapping process and stores in mongodb database

## Automating the scrapping process

    crontab -e
    0 1 * * * /home/user099/samagra/main.py
    
I havn't added this to the python script or api.
This will scrap the website everyday at 01:00AM and store the contents in mongodb.

## Project status

I've completed the following tasks:
* scrapping the data of courses from website (instructor, url, title, layout) The scraping takes around 1 minute, I've tried it through my laptop and a server.
* storing the scrapped data in mongodb database. I'm saving the data after getting the contents in every loop.
* created requested api's
* the api to start the scrapping is /start - you can observe the output from terminal and web interface. It only returns success on completion of process.
* for automating tasks I've added cronjob code in readme of the project.

I've not hosted this on the server as my server was facing issues with package manager and mongodb is not working on it.

As I was doing such task for first time (scrapping and using mongodb) I'm unable to debug the /get api, the code works great on mongodb via terminal but is not returning the data via api. Inconvenience due to this is regretted.