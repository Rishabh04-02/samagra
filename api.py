from sanic import Sanic
from sanic.response import json
import requests 
from pymongo import MongoClient
import os

# Establish connection to mongodb
client = MongoClient("mongodb://localhost:27017/")
db = client["samagra"] # database name - samagra
mycol = db["website"] # collection name

app = Sanic()

@app.route('/')
async def test(request):
    return json({'message': 'Welcome to the application'})

@app.route('get/')
async def getData(request):
	idd = request.args.get('idd')
	data = mycol.find_one({'id': idd})
	return json({'id': idd, 'data': data})

@app.route('start/')
async def startScrap(request):
	this = os.system("python3 main.py")
	return json({'output': 'scrapping Completed'})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)