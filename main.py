import requests
from bs4 import BeautifulSoup
import re
from pymongo import MongoClient

# Establish connection to mongodb
client = MongoClient('localhost', 27017)

db = client['samagra'] # database name - samagra
mycol = db["website"] # collection name

def fetchCourseDetails(idd,url):
	# base url for course pages
	baseurl = 'https://swayam.gov.in'

	courseUrl = baseurl + url
	coursePage = requests.get(courseUrl)

	# Create a BeautifulSoup object
	soupCourse = BeautifulSoup(coursePage.text, 'html.parser')

	# Getting the title
	Title = str(soupCourse.find('title'))
	Title = Title.lstrip('<title>').rstrip('</title>').strip()
	#print(Title)

	# Getting the name of the professor
	Prof = str(soupCourse.find(class_='col-lg-12 col-md-12 col-sm-12 col-xs-12'))
	Prof = Prof.lstrip('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="color:#FFFFFF; font-weight:300">').rstrip('</div>').strip()
	#print(Prof)

	# Getting the Course Description
	Description = str(soupCourse.find(class_='yui-wk-div'))
	Description = Description.lstrip('<div class="yui-wk-div" style="text-align: justify; "><span class="yui-tag-span yui-tag" style="font-size: 14px;" tag="span">').rstrip('</span></div>').strip()
	#print(Description)

	#Getting the course layout
	Layout = str(soupCourse.find('tbody'))
	Layout = Layout.lstrip('<tbody><tr valign="top">').rstrip('</font></p></td></tr></tbody>')
	#print(Layout)

	# Saving data in mongodb database
	post_data = {
	'id': idd,
	'url': url,
	'title': Title,
	'professor': Prof,
	'description': Description,
	'layout': Layout
	}
	result = mycol.insert_one(post_data)
	print(format(result.inserted_id))

# main page of the website which has links of other pages
page = requests.get('https://swayam.gov.in/explorer')

# Create a BeautifulSoup object
soup = BeautifulSoup(page.text, 'html.parser')

# Getting all the scripts
all_scripts = soup.find_all('script')

# Our required script
requiredScript = all_scripts[4]

# Converting data to str for manipulation
new = str(requiredScript)

# slicing not required content
oh = new.find('courses:')
oe = new.find('exam_date:')
owe = slice(oh,oe)
newData = new[owe]

# convert string to list
newdd = re.split(':|,',newData)

# get number of urls
x = newdd.count(' "url"')
urls = []

# Saving the urls to a new list
for i in range(len(newdd)):
	if ' "url"' == newdd[i]:
		urls.append(newdd[i+1])

# Stripping urls of unnecessary characters and fetching details of every url
for i in range(len(urls)):
	urls[i] = urls[i].lstrip(' "')
	urls[i] = urls[i].rstrip('"')
	fetchCourseDetails(i,urls[i])
	print(i,urls[i])
